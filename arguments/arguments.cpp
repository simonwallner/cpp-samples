#include <iostream>

int main(int argc, char *argv[])
{
	std::cout << "reading command line arguments" << std::endl;
	std::cout << "got " << argc << " arguments, which are:" << std::endl;

	for (int i = 0; i < argc; i++)
	{
		std::cout << "\t" << argv[i] << std::endl;
	}
	
	system("PAUSE");
}
