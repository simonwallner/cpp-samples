#include <iostream>
#include <string>

int main()
{
	// the standard string class
	std::string foo = "Hello, world!";
	std::cout << foo << std::endl;
	
	// character arrays are null-terminated
	// see http://en.wikipedia.org/wiki/Null-terminated_string
	char bar[] = {'H', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd', '!'};
	bar[6] = '\0';
	std::cout << bar << std::endl; // "Hello,"
	
	// convert a string to a constant character array
	const char* buz = foo.c_str();
	std::cout << buz << std::endl; // "Hello, World!"

	system("PAUSE");
}