#include <iostream>
#include <string>


class ShallowPerson
{
public:
	const std::string name;
	ShallowPerson* child;


	ShallowPerson(const std::string& _name) : name(_name), child(nullptr) {}
	
	// C++ generates a copy contructor for us (among other things) that looks like
	// ShallowPerson(const ShallowPerson& rhs) : name(rhs.name), child(rhs.child) {}

	// create custom destructor to clean up cild
	~ShallowPerson()
	{
		if(child != nullptr)
			delete child;
	}
		
	std::string hello() const
	{
		if (child != nullptr)
			return name + " (child: " + child->hello() + ")";
		else 
			return name;
	}
};


class DeepPerson
{
public:

	const std::string name;
	DeepPerson* child;
	
	DeepPerson(const std::string& _name) : name(_name), child(nullptr) {}
	
	// provide a custom copy constructor
	DeepPerson(const DeepPerson& rhs) : name(rhs.name + "-clone")
	{
		if (rhs.child != nullptr)
		{
			child = new DeepPerson(*rhs.child);
		}
		else
			child = nullptr;
	}

	DeepPerson& operator=(const DeepPerson& rhs)
	{
		if (this == &rhs)
			return *this;

		if (rhs.child != nullptr)
		{
			child = new DeepPerson(*rhs.child);
		}
		else
			child = nullptr;
		return *this;
	}

	// create custom destructor to clean up child
	~DeepPerson()
	{
		if(child != nullptr)
			delete child;
	}
	
	std::string hello() const
	{
		if (child != nullptr)
			return name + " (child: " + child->hello() + ")";
		else 
			return name;
	}
};

DeepPerson getPerson()
{
	DeepPerson person("Jane Average");
	person.child = new DeepPerson("lil Jane");
	return person;
}

int main()
{
	ShallowPerson* alice = new ShallowPerson("Alice");
	alice->child = new ShallowPerson("Bob");
	std::cout << alice->hello() << std::endl; // Alice (child: Bob)
	
	ShallowPerson* carol = new ShallowPerson(*alice); // invoking generated copy constructor
	std::cout << carol->hello() << std::endl; // Alice (child: Bob)

	delete alice;
	// delete carol; // BOOM! crash due to double deletion of child


	DeepPerson* dave = new DeepPerson("Dave");
	dave->child = new DeepPerson("Eve");
	std::cout << dave->hello() << std::endl; // Dave (child: Eve)
	
	DeepPerson* fred = new DeepPerson(*dave);
	std::cout << fred->hello() << std::endl; // Dave-clone (child: Eve-clone)
	
	delete dave;
	delete fred; // everything is fine now!


	// we also fixed the problem with the assignment operator :D
	DeepPerson jane("");
	jane = getPerson();


	system("PAUSE");
}
