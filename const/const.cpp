#include <iostream>

class SomeClass
{
public:
	int foo;
};

int main()
{
	// we can assign 'normal' variables as expected
	{
		int foo = 42;
		foo = 100;
	}
	
	// const variables cannot be assigned
	{
		const int foo = 42;
		// foo = 100; // error C3892: 'foo' : you cannot assign to a variable that is const
	}
	
	// references cannot be changed, there isnt even a reasonable syntax for it.
	{
		int one = 1;
		int two = 2;
		int& number = one;
		number = two; // not intended!!!
	}
	
	// we can assign pointers
	{
		int one = 1;
		int two = 2;
		int* number = &one;
		number = &two;
	}
	
	// cannot point to const int with int* pointer
	{
		const int one = 1;
		// int* number = &one; // error C2440: 'initializing' : cannot convert from 'const int *' to 'int *'


	}

	// can point to int with const int* pointer
	{	
		int one = 1;
		const int* number = &one;
	}
	
	// pointer to a const object can be changed, the referenced object however
	// cannot.
	{
		int one = 1;
		int two = 2;
		const int* number = &one;
		number = &two;
		
		// *number = 3; // error C3892: 'number' : you cannot assign to a variable that is const
	}
	
	// const pointer cannot be changed, the referenced object however can
	{
		int one = 1;
		int two = 2;
		int* const number = &one;
		// number = &two; // error C3892: 'number' : you cannot assign to a variable that is const
		
		*number = 0;
	}
	
	// once assigned nothing can be changed in a const pointer to a constant
	// object
	{
		int one = 1;
		int two = 2;
		
		const int* const number = &one;
		// number = &two; // error C3892: 'number' : you cannot assign to a variable that is const
		// *number = 0; // error C3892: 'number' : you cannot assign to a variable that is const
	}
	system("PAUSE");
}
