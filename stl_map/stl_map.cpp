#include <iostream>
#include <map>
#include <unordered_map>
#include <string>

int main()
{
	/*
	 * plain old c++99 maps
	 */
	
	// It is practical to use typedefs for containters. It is especially
	// usefull when declaring iterators and avoids smelly code duplication
	typedef std::map<int, std::string> catalogueType;
	
	catalogueType symphonies;
	symphonies[3] = "Eroica";
	symphonies[9] = "Choral";
	symphonies[6] = "Pastoral";
	
	for (catalogueType::const_iterator ci = symphonies.begin();
		ci != symphonies.end(); ci++)
	{
		std::cout << "Symphony Nr " << ci->first << ": " << ci->second << std::endl;
	}
	
	
	typedef std::map<std::string, int> biographyType;
	
	biographyType composers;
	composers["Mozart"] = 1756;
	composers["Beethoven"] = 1770;
	composers["Strauss"] = 1827;
	
	for (biographyType::const_iterator ci = composers.begin();
		ci != composers.end(); ci++)
	{
		std::cout << ci->first << " was born in " << ci->second << std::endl;
	}
	

	/*
	 * C++11 unordered map
	 * Uses a hash table implementation instead of a binary tree in the 
	 * normal map.
	 */
	std::unordered_map<std::string, int> painters;
	
	painters["Vermeer"] = 1632;
	painters["Monet"] = 1814;
	painters["Mondrian"] = 1832;
	
	// C++11 also brings an explicit constant iterator to be used with 'auto'
	for (auto ci = painters.cbegin(); ci != painters.cend(); ci++)
	{
		std::cout << ci->first << " was born in " << ci->second << std::endl;
	}
	
	auto needle = painters.find("Vermeer"); // std::unordered_map<std::string, int>::const_iterator
	if (needle != painters.end())
		std::cout << "We found Vermeer!" << std::endl;
	
	auto needle2 = painters.find("Magritte"); // std::unordered_map<std::string, int>::const_iterator
	if (needle2 == painters.end())
		std::cout << "Could not find Magritte :'(" << std::endl;
	
	system("PAUSE");
	return 0;
}
