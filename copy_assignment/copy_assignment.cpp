#include <iostream>
#include <string>

class Person
{
public:
	Person() : name("") {}
	Person(const std::string& _name) : name(_name) {}
	
	Person(const Person& copy)
	{
		std::cout << "invoking copy contructor..." << std::endl;
		this->name = copy.name;
	}
	
	Person& operator=(const Person& rhs)
	{
		std::cout << "invoking assignment operator..." << std::endl;
		this->name = rhs.name;
		return *this;
	}
	
	void hello()
	{
		std::cout << "Hello, my name is " << name << std::endl;
	}
	
private:
	std::string name;
};

int main()
{
	Person alice("Alice");
	Person alice2(alice); // copy constructor
	Person alice3 = alice; // copy constructor
	
	Person anotherAlice;
	anotherAlice = alice; // assignment operator
	anotherAlice.hello();

	system("PAUSE");
}