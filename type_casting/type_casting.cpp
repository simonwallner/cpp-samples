#include <iostream>
#include <string>

/*
 * see http://www.cplusplus.com/doc/tutorial/typecasting/ for more details
 * samples partly taken from there.
 */

class Person
{
public:
	std::string name;

	Person(std::string _name) : name(_name)
	{
		std::cout << "constructor called" << std::endl;
	}
};

class Student
{
public:
	std::string name;

	explicit Student(std::string _name) : name(_name)
	{
		std::cout << "Student constructor called" << std::endl;
	}
};

class Base {};
class Derived: public Base { };

class PolymorphicBase {
	virtual void dummy() {}
};
class PolymorphicDerived: public PolymorphicBase {
public:
	int foo;
	PolymorphicDerived() : foo(42) {}
	void hello()
	{
		std::cout << "hello " << foo << std::endl;
	}
};

int main()
{
	/*
	 * implicit conversions
	 */
	{
		// implicit conversions of primitive types
		int foo = 3.14f;		// warning C4244: 'initializing' : conversion from 'double' to 'int', possible loss of data
		float bar = 3.14f;
		foo = bar;				// warning C4244: '=' : conversion from 'float' to 'int', possible loss of data
		bool theTruth = foo;	// warning C4800: 'int' : forcing value to bool 'true' or 'false' (performance warning)


		// implicit conversion of class objects
		std::string name("Alice");
		Person alice = name;	// no problem, uses Person constructor
		// Student aliceStudent = name; // can't use Student constructor since it was marked as explicit
	}
	

	/*
	 * explicit type conversion
	 */
	{
		// c-style type explicit casts for privimite types
		int foo = (int)3.14f;
		int bar = int(3.14f);

		// avoid c-style casts on classes or pointers, bad things will happen!
		// use c++ casting instead!

		
		/*
		 * 1. dynamic cast
		 */

		{ // upcast from derived to base class
			Derived derived;
			Base* base = dynamic_cast<Base*>(&derived); 
		}

		{ // uses RRTI to ensure cast is possible
			PolymorphicBase* pBase = new PolymorphicDerived();
			PolymorphicDerived* pAnotherDerived = dynamic_cast<PolymorphicDerived*>(pBase);
			pAnotherDerived->hello();
		}

		{ // nullptr is returned if cast fails
			PolymorphicBase* pBase = new PolymorphicBase();
			PolymorphicDerived* anotherDerived = dynamic_cast<PolymorphicDerived*>(pBase);
			if (anotherDerived == nullptr)
				std::cout << "up cast failed" << std::endl;
		}

		/*
		 * 2. static cast
		 */
		{ // Similar to dynamic_cast, but without checking with the RTTI
			PolymorphicBase* pBase = new PolymorphicBase();
			PolymorphicDerived* anotherDerived = static_cast<PolymorphicDerived*>(pBase);
			if (anotherDerived == nullptr)
				; // not reached unless it was a nullptr from the start
			else
				anotherDerived->hello(); // we are reading garbage, since we are pointing to an incomplete object!
		}

		{ // Usefull and recommended for primitive types though.
			int foo = static_cast<int>(3.14f);
		}

		{ // Can also be used for conversion with explicit contructors
			Student bob = static_cast<Student>(std::string("Bob"));
			Student carol = Student(std::string("Carol")); // that's nicer though!
		}

		/*
		 * 3. reinterpret cast
		 */
		{ // simply reninterpret the data with the new class, no matter how wrong it is!
			Student alice("Alice");
			Base* base = reinterpret_cast<Base*>(&alice); // DON'T DO THIS!

			// can be usefull if you get a blob of data in a void* pointer and know EXACTLY
			// what the data is and what you do.
		}

		/*
		 * 4. const cast
		 */
		{ // strip the constness off a variable, DON'T DO THIS!
			const char foo[] = {'H', 'e', 'l', 'l', 'o'};
			char* bar = const_cast<char*>(foo);
			bar[4] = '\0';
			std::cout << "Hello is now: " << bar << std::endl;
		}


	}



	system("PAUSE");
}
