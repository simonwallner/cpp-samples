#include <iostream>

class A
{
public:
	virtual void hello() const
	{
		std::cout << "hello, this is A" << std::endl;
	}
};

class B : public A
{
public:
	void hello() const
	{
		std::cout << "hello, this is B" << std::endl;
	}
};

void sayHello(A& handle)
{
	handle.hello();
}

int main()
{
	{ // using pointers
		A* handle = new B();
		handle->hello(); // hello, this is B
	}
	
	{ // using references in function parameters
		B handle;
		sayHello(handle);
	}
	
	{ // using a reference
		B b;
		A& handle = b;
		handle.hello();
		
		/* note:
		 * A& handle = B()
		 * does not work, because B() returns a so called 'rvalue' that is
		 * not stored somewhere. After this line would be executed the
		 * temporary rvalue would be destroyed and hence the reference
		 * invalidated
		 * 
		 * On the other hand
		 * const A& handle = B()
		 * works, see (http://stackoverflow.com/questions/2315398/reference-initialization-in-c)
		 * for more details
		 */
	}
}