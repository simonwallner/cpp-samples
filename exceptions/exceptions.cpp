#include <iostream>
#include <string>
#include <stdexcept>

void somethingThatMightFail(int what)
{
	switch (what)
	{
	case 1:
		throw "oops!";
		break;
	case 2:
		throw std::string("oops!");
		break;
	case 3:
		throw std::invalid_argument("does not compute!");
		break;
	case 4:
		throw 42;
	}
}

void throwListed(int what) throw(std::string)
{
	switch (what)
	{
	case 1:
		throw(std::string("all is fine with throwing a string"));
		break;
	case 2:
		throw(42);
		// throwing something that is not listed causes a call to unexpected()
		// which is usually realy, realy, bad!
	}	
}

int main()
{
	try
	{
		// change the number to trigger different exceptions
		somethingThatMightFail(1);
	}
	catch (const char* e)
	{
		std::cout << "exception cought (char*): " << e << std::endl;
	}
	catch (const std::string& e)
	{
		std::cout << "exception cought (string): " << e << std::endl;
	}
	catch (const std::invalid_argument& e)
	{
		std::cout << "exception cought: " << e.what() << std::endl;
	}
	catch (...) // pokemon exception handling, Avoid at all cost!
	{
		std::cout << "somehing we wrong, not sure what!" << std::endl;
	}
	
	
	try
	{
		throwListed(1);
	}
	catch (const std::string& e)
	{
		std::cout << e << std::endl;
	}

	system("PAUSE");
}
