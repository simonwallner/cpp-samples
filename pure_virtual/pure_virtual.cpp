#include <iostream>

class SomePureVirtualClass
{
public:
	SomePureVirtualClass() : foo(41), bar(0) {}
	
	virtual void doOneThing() = 0;
	virtual int doAnotherThing() const = 0;
	// all pure virtual methods must be implemented in base class

protected:
	int foo;
	int bar;
};

class SomeClass : public SomePureVirtualClass
{
public:
	// calling super constructor to init foo and bar
	SomeClass() : SomePureVirtualClass() {}

	void doOneThing()
	{
		foo++;
	}
	
	int doAnotherThing() const
	{
		std::cout << foo << std::endl;
		return bar;
	}
};

int main()
{
	SomeClass foo;
	foo.doOneThing();
	foo.doAnotherThing();
	
	// SomePureVirtualClass bar; // error: cannot declare variable ‘bar’ to be of abstract type ‘SomePureVirtualClass’

	system("PAUSE");
}
