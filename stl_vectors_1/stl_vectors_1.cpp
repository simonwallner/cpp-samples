#include <iostream>
#include <vector>
#include <stdexcept>

/*
 * STL vectors I
 * create, read and write vectors
 */

int main()
{
	/*
	 * Good practice: hide the concrete implementation detail and avoid code
	 * duplication by giving it a practical and funcitonal name.
	 */
	typedef std::vector<float> vertexListType;
	
	// create vector with 5 elements of value 42 
	vertexListType vec(5, 42); 
	std::cout << "size: " << vec.size() << ", capacity: " << vec.capacity() << std::endl;
	// size: 5, capacity: 5
	
	/*
	 * We can just push into the vector, it will be resized automatically 
	 * on the fly
	 */
	for (unsigned int i = 0; i < 10; i++)
	{
		vec.push_back(i);
	}
	
	std::cout << "size: " << vec.size() << ", capacity: " << vec.capacity() << std::endl;
	// size: 15, capacity: 20
	// Performance Hint: if you can try to avoid reallocation of the vector by
	// creating a big enough vector in the first place
	
	/* 
	 * Good practice: use the constant iterator runless you really want to 
	 * modify the collection. In most cases you don't :D
	 */
	for(vertexListType::const_iterator ci = vec.cbegin(); ci != vec.cend(); ci++)
	{
		std::cout << (*ci) << std::endl;
	}
	
	// use the safe .at() method to avoid out-of-bounds reads
	try {
		std::cout << vec.at(100); 
	}
	catch (const std::out_of_range &e) {
		std::cout << "cought index out of bounds exception: " << e.what() << std::endl;
	}

	system("PAUSE");
}