#include <iostream>

/**
 * Simple 2D Vector with value semantics
 * i.e. once set the value of the vector cannot be changed.
 * Adding two vectors returns a new vector holding the result.
 */
class MyVector
{
public:
	const float x;
	const float y;

	MyVector(float _x, float _y) : x(_x), y(_y) {}
	
	MyVector operator+(const MyVector& rhs) const
	{
		return MyVector(this->x + rhs.x, this->y + rhs.y);
	}
	
private:
	// hide the assignment contructor
	MyVector operator=(const MyVector& rhs);
};

MyVector operator+ (int lhs, MyVector& rhs)
{
	return MyVector(lhs + rhs.x, lhs + rhs.y);
}

int main()
{
	/**
	 * testing the + operator for MyVector
	 */
	MyVector v1(1.0f, 2.0f);
	MyVector v2(2.0f, 3.0f);
	MyVector v3 = v1 + v2;
	
	std::cout << v3.x << ", " << v3.y << std::endl;
	
	/**
	 * testing the + operator for int and MyVector
	 */
	MyVector v4 = 42 + v1;
	std::cout << v4.x << ", " << v4.y << std::endl;

	system("PAUSE");
}
