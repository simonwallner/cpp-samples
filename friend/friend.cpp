#include <iostream>
#include <string>

class SecretClass
{
public:
	SecretClass() : secret("Secret class is secret!") {}
	
	// Declare the function operatoc<< as a friend so that it can access
	// private variables.
	friend std::ostream& operator<<(std::ostream& out, SecretClass& sec);
	
private:
	const std::string secret;
};

// overloading the operator<< 
std::ostream& operator<<(std::ostream& out, SecretClass& sec)
{
	return out << sec.secret;
}

int main() 
{
	SecretClass pst;
	std::cout << pst << std::endl; // "Secret class is secret!"
	
	system("PAUSE");
}