#include <iostream>
#include <cstring>

template <typename T >
T max(T v1, T v2)
{
	if (v1 >= v2)
		return v1;
	else
		return v2;
}

/* 
 * Create a concrete specialisation of the template to use real string
 * comparisson instead of only comparing pointer adresses.
 */
template<>
const char* max<const char*>(const char* val1, const char* val2)
{ 
  // compare strings, not pointers
	if(std::strcmp(val1, val2) >= 0) 
		return val1; 
	else 
		return val2; 
}


int main()
{
	std::cout << max(1, 2) << std::endl; // 2
	std::cout << max(2.0f, 4.5f) << std::endl; // 4.0
	
	// std::cout << max(1, 2.5f) << std::endl;
	// error C2782: 'T max(T,T)' : template parameter 'T' is ambiguous
	
	std::cout << max<int>(1, 2.5f) << std::endl; // 2
	std::cout << max<float>(1, 2.5f) << std::endl; // 2.5

	// try commenting out the const char* specialisation above and see the
	// different result.
	std::cout << max<const char*>("Vienna", "Paris") << std::endl; // Vienna

	system("PAUSE");
}