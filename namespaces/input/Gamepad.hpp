#ifndef MYGAME_INPUT_GAMEPAD_HPP_
#define MYGAME_INPUT_GAMEPAD_HPP_

namespace myGame
{
	namespace input
	{
		class Gamepad
		{
			void pollDevice();
		};
	}
}

#endif
