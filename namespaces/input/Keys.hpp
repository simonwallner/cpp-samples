#ifndef MYGAME_INPUT_KEYS_HPP_
#define MYGAME_INPUT_KEYS_HPP_

namespace myGame
{
	namespace input
	{
		class Keys
		{
			unsigned int getKeyCode();
		};
	}
}

#endif
