#include <iostream>

#include "scene/PolyMesh.hpp"
#include "input/Gamepad.hpp"
#include "input/Keys.hpp"

using namespace myGame::scene;
using myGame::input::Gamepad;

int main()
{
	PolyMesh mesh();
	Gamepad pad();
	myGame::input::Keys keyboard();
	
	std::cout << "keep calm and carry on!" << std::endl;

	system("PAUSE");
}