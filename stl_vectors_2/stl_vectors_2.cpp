#include <iostream>
#include <vector>

/*
 * STL Vectors II
 * Using vectors to interface with c style API calls
 */
void someCAPIFunction(const float* data, unsigned int length)
{
	if (data != nullptr)
	{
		for (unsigned int i = 0; i < length; i++)
		{
			std::cout << "performing magic on data element: " << data[i] << std::endl;
		}
	}
}

int main()
{
	std::vector<float> vec(10, 1.34159);
	
	someCAPIFunction(static_cast<float*>(&(vec[0])), vec.size());
	someCAPIFunction(&vec[0], vec.size()); // shorter but less 'clean'

	system("PAUSE");
}